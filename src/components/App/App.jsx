import React, { useEffect } from 'react';
import { Layout } from '../../Layout/Layout';

import { useDispatch } from 'react-redux';
import { getData } from '../../redux/extraReducers/getData';
import {
  setFavorites,
  setProductsInBasket,
} from '../../redux/reducers/products';

const favoriteIdsFromStorage = localStorage.getItem('favorites');
const basketIdsFromStorage = localStorage.getItem('basket');

export const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);


  useEffect(() => {
    if (favoriteIdsFromStorage) {
      dispatch(setFavorites(JSON.parse(favoriteIdsFromStorage)));
    }

    if (basketIdsFromStorage) {
      dispatch(setProductsInBasket(JSON.parse(basketIdsFromStorage)));
    }
  }, [dispatch]);

  return <Layout />;
};
