import React from 'react';
import styles from './Main.module.scss';

import { MainLeft } from '../MainLeft/MainLeft';
import { MainRight } from '../MainRight/MainRight';

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <MainLeft />
      <MainRight />
    </div>
  );
};
