import React from 'react';
import styles from './Favorites.module.scss';
import { Product } from '../../components/Product/Product';

import { useSelector } from 'react-redux';

const getProducts = (state) => state.products;

export const Favorites = () => {
  // get product in favorites
  const { items, favorites } = useSelector(getProducts);

  // set product in favorites
  const productsFavorites = items.filter((product) =>
    favorites.includes(product.id)
  );

  // check flag if favorites is empty
  const emptyFavorites = !productsFavorites.length;

  return (
    <div className={styles.Favorites}>
      {emptyFavorites && <h1>Товар відсутній</h1>}
      {!emptyFavorites && (
        <ul className={styles.FavoritesList}>
          {productsFavorites.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
